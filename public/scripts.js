const app = document.getElementById('root');

const logo = document.createElement('img');
logo.src = 'logo.png';

const container = document.createElement('div');
container.setAttribute('class', 'container');

app.appendChild(logo);
app.appendChild(container);


var request = new XMLHttpRequest()
request.open('GET', 'https://ghibliapi.herokuapp.com/films', true)
request.onload = function() {

  var respuesta = JSON.parse(this.response)
  if (request.status >= 200 && request.status < 400) {
    respuesta.forEach(function(objeto, indice) {
      var pelicula = document.createElement('div');
      pelicula.setAttribute('class', 'pelicula');

      var titulo = document.createElement('div');
      titulo.setAttribute('class', 'pelicula__titulo');
      titulo.textContent = objeto.title;

      var descripcion = document.createElement('div');
      descripcion.setAttribute('class', 'pelicula__descripcion');
      descripcion.textContent = objeto.description;

      var director = document.createElement('div');
      director.setAttribute('class', 'pelicula__director');
      director.textContent = objeto.director;

      var foto = document.createElement('div');

      var img = document.createElement('img');
      img.src = 'https://i.ebayimg.com/images/g/yGEAAOSwD8ZZ8u1b/s-l300.jpg';



      container.appendChild(pelicula);
      pelicula.appendChild(titulo);
      pelicula.appendChild(descripcion);
      pelicula.appendChild(director);
      pelicula.appendChild(foto);
      foto.appendChild(img);
    });
  } else {
    const errorMessage = document.createElement('marquee');
    errorMessage.textContent = 'No jala carnal';
    app.appendChild(errorMessage);
  }
}

request.send();
